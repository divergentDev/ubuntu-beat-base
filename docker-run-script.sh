#!/bin/bash

docker rm $(docker ps -a -q) > out 2>error

docker build -t ubuntu-beat-base .;

sleep 3;

docker run -p 5602:5602 -p 9201:9201 -v /var/run/docker.sock:/var/run/docker.sock -e SERVICE_NAME=ktstaging -e HOST_IP=167.99.1.52 -e KIB_PORT=5601 -e ES_PORT=9200 --name beats ubuntu-beat-base

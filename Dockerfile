#Use ubuntu build as the base image for the elasticsearch installation.
FROM ubuntu:18.10

ENV startup_file="initscript.sh"
ENV metricbeat_config_dir=""
ENV filebeat_config_dir=""
ENV heartbeat_config_dir=""
ENV HOST_IP=""
ENV KIBANA_PORT="5601"
ENV ES_PORT="9200"
ENV APACHE_PORT=""
ENV metricbeat_module_dir="/usr/share/metricbeat/modules.d"
ENV metricbeat_conf="/etc/metricbeat/metricbeat.yml"
ENV metricbeat_dashboard_dir="/etc/metricbeat/dashboards"

ENV APACHE_METRICS=""
ENV MYSQL_METRICS=""

ENV DB_USER=""
ENV DB_PASS=""
ENV DB_PORT=""

#update apt-get
RUN apt-get -y update && apt-get -y upgrade

#install wget AND transport https
RUN apt-get install -y wget apt-transport-https

#install gnupg
RUN apt-get -y install gnupg

#install the public signing key
RUN wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -

#Save the repository definition to /etc/apt/sources.list.d/elastic-6.x.list:
RUN echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-6.x.list

#install vim and less
RUN apt remove -y vim-tiny && \
    apt install -y vim && \
    apt-get install -y less

#install metricbeat, heartbeat & filebeat
RUN apt-get -y update && \
    apt-get -y install metricbeat && \
    apt-get -y install heartbeat && \
    apt-get -y install filebeat

#copy the metricbeat config file to the correct directory
COPY metricbeat.yml $metricbeat_conf

#copy the docker yml config file
COPY *.yml.disabled $metricbeat_module_dir/
COPY system.yml $metricbeat_module_dir/

RUN mkdir -p $metricbeat_dashboard_dir && \
    chmod 755 $metricbeat_dashboard_dir

COPY mb_dashboards.zip $metricbeat_dashboard_dir/

#transfer the initscript to the container
COPY $startup_file /

#switch working directory
WORKDIR /usr/share/metricbeat/kibana/6/dashboard/

#delete dashboards that we don't need
RUN rm -f Metricbeat-golang-overview.json Metricbeat-haproxy-backend.json Metricbeat-haproxy-frontend.json Metricbeat-haproxy-overview.json Metricbeat-haproxy-visualizations.json Metricbeat-kubernetes-apiserver.json Metricbeat-kubernetes-overview.json Metricbeat-mongodb-overview.json Metricbeat-rabbitmq-overview.json Metricbeat-redis-overview.json Metricbeat-uwsgi-overview.json metricbeat-nginx-overview.json metricbeat-windows-service.json

WORKDIR /usr/share/metricbeat/kibana/5/dashboard

RUN rm -f Metricbeat-uWSGI.json Metricbeat-Redis.json Metricbeat-Rabbitmq.json Metricbeat-MongoDB.jsond1f1f9e0-1b1c-11e7-b09e-037021c4f8df.json f2dc7320-f519-11e6-a3c9-9d1f7c42b045.json

EXPOSE 5602
EXPOSE 9201

ENTRYPOINT ["/initscript.sh"]


#!/bin/bash

#set metricbeat configurations
sed -ri "s/app/$SERVICE_NAME/g" $metricbeat_conf 
sed -ri "s/server_ip/$HOST_IP/g" $metricbeat_conf
sed -ri "s/kibana_port/$KIB_PORT/g" $metricbeat_conf
sed -ri "s/es_port/$ES_PORT/g" $metricbeat_conf
sed -ri "s/apache_host/$HOST_IP/g" $metricbeat_module_dir/apache.yml.disabled
sed -ri "s/apache_port/$APACHE_PORT/g" $metricbeat_module_dir/apache.yml.disabled
sed -ri "s/DB_USER/$DB_USER/g" $metricbeat_module_dir/mysql.yml.disabled
sed -ri "s/DB_PASS/$DB_PASS/g" $metricbeat_module_dir/mysql.yml.disabled
sed -ri "s/HOST_IP/$HOST_IP/g" $metricbeat_module_dir/mysql.yml.disabled
sed -ri "s/DB_PORT/$DB_PORT/g" $metricbeat_module_dir/mysql.yml.disabled

#enable the system module
#metricbeat modules disable system | cat
metricbeat modules enable docker | cat

if [ "$APACHE_METRICS" = "true" ]; then
   echo "enabling apache metrics"
   metricbeat modules enable apache | cat
fi

if [ "$MYSQL_METRICS" = "true" ]; then
   echo "enabling mysql metrics"
   metricbeat modules enable mysql | cat
fi

#setup metricbeat dashboards
metricbeat setup | cat

#start metricbeat
service metricbeat start

tail -f /dev/null
